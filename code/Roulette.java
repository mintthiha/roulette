import java.util.Scanner;

public class Roulette {
  public static void main(String args[]){
    Scanner scan = new Scanner(System.in);
    RouletteWheel rouletteWheel = new RouletteWheel();
    int currentCash = 1000;
    boolean isGameOver = false;
    int totalMoneyWon = 0;
    int totalMoneyLost = 0;

    while(!isGameOver){
      int betChosen = 0;
      int numberChosen = 0;
      boolean ifWon = false;
      System.out.println("Would you like to make a bet? (yes or no)");
      String willYoubet = scan.next();
      if (willYoubet.equals("yes")){
        System.out.println("Awesome, how much would you like to bet? (enter the amount of cash you would like to bet)");
        boolean isGoodbet = false;
        while(!isGoodbet){
          betChosen = scan.nextInt();
          if (betChosen > currentCash || betChosen < 0) {
            System.out.println("Please put an appropriate amount of cash.");
          } else{
            isGoodbet = true;
          }
        }

        boolean isGoodNumber = false;
        System.out.println("Nice, now what is the special number you are betting on? (pick a number that is 0 to 36, both numbers included)");
        while (!isGoodNumber){
          numberChosen = scan.nextInt();
          if(numberChosen > 36 || numberChosen < 0){
            System.out.println("The number needs to be higher or equals than 0 and not higher than 36, try again!");
          }
          else {
            isGoodNumber = true;
          }
        }

        rouletteWheel.spin();
        ifWon = checkIfWon(numberChosen, rouletteWheel.getValue());
        if(ifWon == true){
          betChosen = betChosen * 35;
          totalMoneyWon += betChosen;
          System.out.println("Congratulations, you have won " + betChosen + "$");
          currentCash =+ betChosen;
          System.out.println("Your current cash right now is " + currentCash + "$");
          System.out.println();
        }
        else {
          System.out.println("Unfortunately, you have lost...");
          totalMoneyLost += betChosen;
          currentCash -= betChosen;
          System.out.println("Your current cash right now is: "+ currentCash + "$");
          System.out.println();
        }
        
        if(currentCash == 0){
          System.out.println("It seems like you ran out of money! Too bad...");
          System.out.println();
          isGameOver = true;
        } else {
          System.out.println("Would you like to play again?");
          String answer = scan.next();
          if (answer.equals("no")) {
          System.out.println("Understandable, have a great day!");
          isGameOver = true;
          }
          else {
            System.out.println("Alright, here we go again!");
            System.out.println();
          }
        }
      }
      else{
        System.out.println("Okay then! Have a nice day!");
        isGameOver = true;
      }
    }
    System.out.println("Amount of cash won: " + totalMoneyWon + "$");
    System.out.println("Amount of cash lost: " + totalMoneyLost + "$");
    System.out.println("You walked away with: " + currentCash + "$");
    scan.close();
  }

  public static boolean checkIfWon(int numberChosen, int numberGenerated){
    boolean ifWon = false;
    if (numberChosen == numberGenerated){
      ifWon = true;
    }
    return ifWon;
  }
}